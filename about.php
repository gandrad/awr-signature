<?php include 'property/header.php';?>


<div id="content" style="padding-top: 160px;">

    <div class="container">
        <div class="row" style="padding: 40px 0px 40px 0px;">
            <div class="col-xs-12">
                <h2>Who we are</h2>
                <div class="row">
                    <div class="col-md-6">
                        <p>We are a Multinational Real Estate Company with operations in UK-London, and now in Bangladesh.</p>
                        <p>Our employees are the key to success. Their passion, pioneering spirit and professionalism have helped us to expand crossing our border.</p>
                        <p>Each individual employee in every functional area and in every position helps to write our success story. We work together as a team to build a successful future.</p>
                        <p>The AWR Real Estate Team is committed to delivering a top-notch real estate experience and we are prepared to &quot;go the extra mile&quot; to achieve success for all of our clients.</p>
                    </div>
                    <div class="col-md-6"><img class="img-responsive" style="float: right;" title="Who we are" src="images/slider/s2.jpg" alt="Photo of one of our properties" /></div>


                </div>



                <div id="breakpoints">
                    <div class="visible-xs"></div>
                    <div class="visible-sm"></div>
                    <div class="visible-md"></div>
                    <div class="visible-lg"></div>
                </div>


            </div>
        </div>
    </div>
</div>
<?php include 'property/footer.php';?>

<style type="text/css">
    .jbx-ui #content {
        margin-bottom: 25px;
    }
    
</style>
<script>
document.getElementById("about").style.color = 'rgb(194, 171, 96)';    
</script>