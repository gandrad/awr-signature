<?php include 'property/header.php';?>


    <div id="content">

        <div class="row cycle-slideshow" id="slider">
        <article data-cycle-title="NIB Tower"
                     data-cycle-link="awr-nib-tower.html"
                     data-cycle-tag="">
                <img src="images/slider/10.jpg">
             </article>


            <article data-cycle-title=" Usman Tower"
                     data-cycle-link="awr-usman-tower.html"
                     data-cycle-tag="">
                <img src="images/slider/s1.jpg">
            </article>
            <article data-cycle-title="Road 83"
                     data-cycle-link="awr-road-83.html"
                     data-cycle-tag="">
                <img src="images/slider/s5.jpg">
            </article>
             <article data-cycle-title="Road 47"
                     data-cycle-link="#"
                     data-cycle-tag="">
                <img src="images/slider/s2.jpg">
            </article>
            <div class="cycle-overlay container hidden-xs"></div>
            <div class="controls container">
                <a href="#" id="slider-prev"></a>
                <a href="#" id="slider-next"></a>
            </div>
            <div class="cycle-pager"></div>
        </div>

        <div class="container-fluid">
            <div class="row" id="mixitup">


<!--                 <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 mix">
                    <a href="awr-nib-tower.html" title="MERIVALE COLLECTION, SW15">
                    <article style="background-image: url('images/slider/s6.jpg');">

                        <div class="mask"></div>

                       
                        <span class="tag bevel green br">Completed</span> 

                        <div class="meta bevel dark tr">
                            <h2>AWR NIB TOWER</h2>
                            <address>Banani </address>
                            
                                More <span class="arrow"></span>
                           
                        </div>


                    </article></a>
                </div> -->
             <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 mix visible-lg">
                    <a href="awr-usman-tower.php" title="SPHERE APARTMENTS, E3">
                    <article style="background-image: url('images/slider/s1.jpg');">

                        <div class="mask"></div>

                        <span class="tag bevel green br">FOR SALE </span>

                        <div class="meta bevel dark tr">
                            <h2>Usman Tower</h2>
                            <address>Gulshan 1 </address>
                            
                                More <span class="arrow"></span>
                            
                        </div>

                    </article></a>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 mix hidden-sm">
                <a href="awr-road-83.php" title="THE OLD FORD APARTMENTS, E3">
                    <article style="background-image: url('images/slider/s3.jpg');">

                        <div class="mask"></div>

                        <span class="tag bevel green br">FOR SALE </span>

                        <div class="meta bevel dark tr">
                            <h2>ROAD 83</h2>
                            <address>North Gulshan</address>
                            
                                More <span class="arrow"></span>
                            
                        </div>

                    </article></a>
                </div>

<!--                 <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 mix">
                    <a href="" title="ARIANA APARTMENTS, SW6">
                    <article style="background-image: url('images/slider/s8.jpg');">

                        <div class="mask"></div>

                        <span class="tag bevel green br">FOR SALE </span>

                        <div class="meta bevel dark tr">
                            <h2>ROAD 47</h2>
                            <address>Gulshan 2</address>
                            
                                More <span class="arrow"></span>
                            
                        </div>

                    </article></a>
                </div> -->


            </div>
        </div>


    </div>

 <?php include 'property/footer.php';?>

 <script>
    document.getElementById("ongoing").style.color = 'rgb(194, 171, 96)'; 
</script>