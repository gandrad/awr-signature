<?php include 'property/header.php';?>

    <div id="content">

<div class="row" id="pre-dev-slider" style="display: none;">
    <div class="mask"></div>
    <div class="cycle-overlay container hidden-xs"></div>
    <div class="controls container">
        <a href="#" id="pre-dev-slider-prev"></a>
        <a href="#" id="pre-dev-slider-next"></a>
    </div>
</div>
<div class="row cycle-slideshow" id="slider">
            <div class="mask"></div>
            <div class="cycle-overlay container hidden-xs"></div>
            <div class="controls container">
                <a href="#" id="slider-prev"></a>
                <a href="#" id="slider-next"></a>
            </div>
            <div class="cycle-pager"></div>
            <article data-cycle-title=""
                 data-cycle-desc=""
                 class="">
                <img src="images/slider/image_coming_soon.png">
            </article>
</div> 

<div class="container">
    <div class="row">
        </br>
        </br></br>

        <div class="col-md-3 col-lg-2" id="project-links">
            <div class="link-group">
                                <a href="#" title="Project overview" id="view-image-gallery" class="" style="color: #c2ad53;">Project overview</a>
                            </div>
            <div class="link-group">
                <a class="back-link" href="index.html" title="Back to Portfolio"><span class="arrow"></span>Back to Home</a>            </div>
        </div>
        
        <div class="col-md-6 col-lg-7" id="overview">
            <h2>Road 83<br>
                <address>Plot - 9, Road - 83, North Gulshan, Dhaka.</address>
            </h2>
            <p>Storied: 9 Floors(GF+8)<br />Status: Ongoing</p>
            <p>It is situated in the tranquil area of North Gulshan just opposite of the Ladies park where you will get peace of mind, free from all the noise and commotion of city life.</p>
            <p>The apartment is moments away with all the shops and restaurants for complete living viewing. It is also easily accessible to a number of excellent local schools.</p>
            <p>For more information please contact our Estate Agents at <strong><a href="mailto:info@awrsignature.com">info@awrsignature.com</a> </strong>.</p>       

        </div>
        
        <div class="col-md-3"><div id="map_canvas"></div></div>
    </div>
</div>

<script>
    var width = $(window).width();
    var timer;
    var initSlideshow = function() {
        $('#slider article').width($('#slider .controls').width());
        $('#pre-dev-slider article').width($('#slider .controls').width());
        $('#slider article').height($('#slider .controls').width() / 2);
        $('#pre-dev-slider article').height($('#slider .controls').width() / 2);
        $('#slider').height($('#slider .controls').width() / 2);
        $('#pre-dev-slider').height($('#slider .controls').width() / 2);
        
        var offset        = width >= 768 ? (($('#slider').width() - $('.controls').width()) / 2) - 15 : 0;
        var carouselWidth = width >= 768 ? $('.controls').width() + 30 : $('.controls').width();
        
        $('#slider').cycle({
            fx: 'carousel',
            carouselSlideDimension: carouselWidth,
            carouselOffset: offset,
            timeout: 0,
            duration: 2000,
            swipe: true,
            next: '#slider-next',
            prev: '#slider-prev',
            slides: '> article',
            log: false
        }).on('cycle-update-view', function(event, optionHash, slideOptionsHash, currentSlideEl) {
            if (!currentSlideEl.getAttribute('data-cycle-title')) {
                slideOptionsHash.overlayTemplate = '<div></div>';
            } else { 
                slideOptionsHash.overlayTemplate = '<div class="row no-gutter"><div class="col-md-4 overlay-container"><div class="bevel dark br"><h2>{{title}}</h2><p>{{desc}}</p></div></div></div>';
            }
        });
        
        $('#pre-dev-slider').cycle({
            fx: 'carousel',
            carouselSlideDimension: carouselWidth,
            carouselOffset: offset,
            timeout: 0,
            duration: 2000,
            swipe: true,
            next: '#pre-dev-slider-next',
            prev: '#pre-dev-slider-prev',
            slides: '> article',
            log: false
        }).on('cycle-update-view', function(event, optionHash, slideOptionsHash, currentSlideEl) {
            if (!currentSlideEl.getAttribute('data-cycle-title')) {
                slideOptionsHash.overlayTemplate = '<div></div>';
            } else { 
                slideOptionsHash.overlayTemplate = '<div class="row no-gutter"><div class="col-md-4 overlay-container"><div class="bevel dark br"><h2>{{title}}</h2><p>{{desc}}</p></div></div></div>';
            }
        });
        
        $('#slider .mask').hide();
        $('#pre-dev-slider .mask').hide();
    };
    
    var initMap = function() {
        var styles       = [ { "stylers": [ { "weight": 2 }, { "saturation": -100 } ] },{ } ];
        var latlngValues = '51.51748809999999,-0.028373900000019603'.split(',');
        var latlng       = new google.maps.LatLng(parseFloat(latlngValues[0]), parseFloat(latlngValues[1]));
        var options      = {
            zoom: 15,
            center: latlng,
            disableDefaultUI: true,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        
        var map = new google.maps.Map(document.getElementById('map_canvas'), options);
        map.setOptions({styles: styles});
        new google.maps.Marker({
            position: latlng, 
            map: map,
            icon: '/packages/jbx_ipedevelopments/themes/ipedevelopments/img/map-marker.png'
        });
        google.maps.event.addDomListener(window, 'resize', function() {
            map.setCenter(latlng);
        });
    };
    
    $(window).on('resize', function() {
        var newWidth = $(window).width();
        clearTimeout(timer);
        if (width !== newWidth) {
            width = newWidth;
            $('#slider .mask, #pre-dev-slider .mask').show();
            if ($('#slider').data('cycle.opts') !== undefined) {
                $('#slider').cycle('destroy');
            }
            $('#pre-dev-slider').hide();
            if ($('#pre-dev-slider').data('cycle.opts') !== undefined) {
                $('#pre-dev-slider').cycle('destroy');
            }
            $('#project-links .active').removeClass('active');
            $('#project-links #view-image-gallery').addClass('active');
            timer = setTimeout(function() { initSlideshow(); }, 1000);
        }
    });
    
    $(document).ready(function() {
        initSlideshow();
            
        $('#slider').on('click', '.textarea', function() {
            var opts = $('#slider').data('cycle.API').getSlideOpts();
            var href = opts.cycleHref;
            window.location.href = href;
        });
            
        $('#pre-dev-slider').on('click', '.textarea', function() {
            var opts = $('#pre-dev-slider').data('cycle.API').getSlideOpts();
            var href = opts.cycleHref;
            window.location.href = href;
        });
        
        $('#view-pre-dev-image-gallery').on('click', function() {
            $('#slider').hide();
            $('#pre-dev-slider').fadeIn('fast');
            $('#project-links .active').removeClass('active');
            $(this).addClass('active');
            return false;
        });
        
        $('#view-image-gallery').on('click', function() {
            $('#pre-dev-slider').hide();
            $('#slider').fadeIn('fast');
            $('#project-links .active').removeClass('active');
            $(this).addClass('active');
            return false;
        });
    });
</script>

    </div>

<?php include 'property/footer.php';?>

 <script>
    document.getElementById("ongoing").style.color = 'rgb(194, 171, 96)'; 
</script>