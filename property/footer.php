
        <div class="container-fluid footer">
            <div class="col-md-9 footerDescription">
                <img src="images/logo.png" class="img-responsive" alt="" width="120" height="50">
                <h3>DISCLAIMER</h3>
                <p>All details contained within this sales information are correct at the time of production. However, in the interest of continuous improvement and to meet market conditions, the builder reserves the right to modify plans, exteriors, specifications,
                    and products without notice or obligation. Actual usable floor space may vary from stated floor area. Any CGIs depicted are an artist’s concept of the completed building and/or its interiors only. The content contained within these
                    particulars may not be current and can change at any time without notice.</p>
            </div>
            <div class="col-md-3 FooterGnR">
                <p class="text-center">SITE DESIGN : G&R</p>
            </div>
        </div>

        <div id="breakpoints">
            <div class="visible-xs"></div>
            <div class="visible-sm"></div>
            <div class="visible-md"></div>
            <div class="visible-lg"></div>
        </div>


    </div>

    <script>
        var width = $(window).width();
        var timer;
        var initSlideshow = function() {
            $('#slider article').width($('#slider .controls').width());
            $('#slider article').height($('#slider .controls').width() / 2);
            $('#slider').height($('#slider .controls').width() / 2);

            var offset = width >= 768 ? (($('#slider').width() - $('.controls').width()) / 2) - 15 : 0;
            var carouselWidth = width >= 768 ? $('.controls').width() + 30 : $('.controls').width();

            $('#slider').cycle({
                fx: 'carousel',
                carouselSlideDimension: carouselWidth,
                carouselOffset: offset,
                timeout: 0,
                duration: 2000,
                swipe: true,
                next: '#slider-next',
                prev: '#slider-prev',
                slides: '> article',
                log: false
            }).on('cycle-update-view', function(event, optionHash, slideOptionsHash, currentSlideEl) {
                if (!currentSlideEl.getAttribute('data-cycle-title')) {
                    slideOptionsHash.overlayTemplate = '<div></div>';
                } else {
                    slideOptionsHash.overlayTemplate = '<div class="container"><div class="row no-gutter"><div class="col-md-4 overlay-container"><div class="bevel dark tr"><h2>{{title}}</h2><p>{{tag}}</p><a href="{{link}}">View project details <span class="arrow"></span></div></div></div></div>';
                }
            });

            $('#slider .mask').hide();
        };

        $(window).on('resize', function() {
            var newWidth = $(window).width();
            clearTimeout(timer);
            if (width !== newWidth) {
                width = newWidth;
                $('#slider .mask').show();
                if ($('#slider').data('cycle.opts') !== undefined) {
                    $('#slider').cycle('destroy');
                }
                $('#project-links .active').removeClass('active');
                $('#project-links #view-image-gallery').addClass('active');
                timer = setTimeout(function() {
                    initSlideshow();
                }, 1000);
            }
        });

        $(document).ready(function() {
            initSlideshow();
        });
    </script>
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/ccm.base.js"></script>
    <script src="js/jquery.cycle.js"></script>
    <script src="js/site.js"></script>
    <script type="text/javascript" src="js/jquery.ui.js"></script>
    <script type="text/javascript" src="js/jquery.mixitup.min.js"></script>
    <script src="http://www.ipe-developments.com/packages/jbx_ipedevelopments/themes/ipedevelopments/js/respond.min.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.js"></script>

    <script>
        $(document).ready(function() {
            $('#mixitup').mixItUp();
        });
    </script>


</body>

</html>