<?php 
function e($src){
  $root = dirname(dirname(__FILE__));
  $mtime = filemtime($root."/".$src);
  echo $src."?v={$mtime}";
} 
?>

<!DOCTYPE html>
<html>

<meta http-equiv="content-type" content="text/html;charset=UTF-8" />

<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge;chrome=1">
    <meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0, width=device-width">

    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <title>AWR Signature</title>
    <meta name="description" content="" />
    <meta name="generator" content="concrete5 - 5.6.3.1" />

    <link rel="stylesheet" type="text/css" href="<?php e('css/ccm.base.css') ?>" />


    <!-- Site Header Content //-->
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600">
    <link rel="stylesheet" href="<?php e('fonts/stylesheet.css') ?>">
    <link rel="stylesheet" href="<?php e('css/jbx-ui.css') ?>">
    <link rel="stylesheet" href="http://www.ipe-developments.com/packages/jbx_ipedevelopments/themes/ipedevelopments/css/screen.css">
    <link rel="stylesheet" href="<?php e('css/responsive.css') ?>">
    <link rel="stylesheet" href="<?php e('css/typography.css') ?>">
    <link rel="stylesheet" href="<?php e('css/style.css') ?>">


    <script>
        $(window).load(function() {
        $(".se-pre-con").fadeOut("slow");;
    });   
    </script>

</head>

<body id="home" class="home">
    <div class="se-pre-con"></div>

    <div class="jbx-ui" id="awr-index">

        <header>
            <div class="container">
                <a href="index.php" class="logo" title="IPE Developments :: Home">
                    <img src="images/logo.png" style=" width: 190px;">
                    <h1 class="hidden-xs">
                        AWR Signature<br> Boutique living
                    </h1>
                </a>


                <button type="button" class="navbar-toggle collapsed mobile-menu-btn visible-xs visible-sm" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>


                <div class="row no-gutter nav">
                    <div class="col-md-2 col-md-offset-6">
                        <nav>
                            <a id="completed" href="completed.php" target="_self">Completed</a>
                            <a id="ongoing" href="ongoing.php" target="_self" class="">Ongoing</a>
                            <a id="upcoming" href="upcoming.php" target="_self">Upcoming</a>

                        </nav>
                    </div>

                    <div class="col-md-2">
                        <nav>
                            <a id="about" href="about.php" target="_self" class="">About Us</a>
                            <a id="contact" href="contact.php" target="_self" class="">Contact</a>

                            <nav>
                    </div>
                    <div class="col-md-2">
                        <nav>
                            <a id="buyer" href="buyer.php" target="_self" class="black_back">Buyer</a>
                            <a id="landowner" href="landowner.php" target="_self" class="">Landowner</a>
                            <nav>
                    </div>
                </div>

            </div>
        </header>