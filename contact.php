<?php include 'property/header.php';?>

    <div id="content" style="padding-top: 160px;">

<div class="container">
    <div class="row" id="featured-image">
        <div class="col-xs-12">
          <!--<div id="map_canvas"></div>-->
          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d14601.594577827864!2d90.39848169999999!3d23.804419400000004!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3755c70ff6a58f15%3A0x441fd57f6c41042b!2s9+Rd+No+25%2C+Dhaka!5e0!3m2!1sen!2sbd!4v1494847740071" width="1140" height="330" frameborder="0" style="border:0" allowfullscreen></iframe>
          </div>
    </div>

    <div class="row">
        <div class="col-xs-12 col-md-4">



            <p><strong></strong><br /> Dhaka Office: House 9, Road 25/A, Block A, <br />Banani, Dhaka 1213</p>
            <p><strong></strong><br /> London Office: 4th floor, 73 New Bond Street, <br />Mayfair, London W1S 1RS</p>

<p><strong></strong><br />Landline:</p>
<p><a title="Call us" href="tel:8802 55033655">+8802 55033655</a></p>
<p><a title="Call us" href="tel:8802 55033655">+8802 55033656</a></p>
<p><strong></strong><br />Mobile:</p>
<p><a title="Call us" href="tel:8802 55033655">+88 01811 409600</a></p>
<p><a title="Call us" href="tel:8802 55033655">+88 01811 409601</a></p>
<p><a title="Call us" href="tel:8802 55033655">+88 01811 409602</a></p>
<p><a title="Email us" href="mailto:info@awr.com.bd" target="_blank">info@awrsignature.com</a></p>
<p> </p>
<p> </p>
<script type="text/javascript" src="../../platform.linkedin.com/in.js">// <![CDATA[
 lang: en_US
// ]]></script>
<script type="text/javascript" data-id="10638543"></script>        </div>
        <div class="col-xs-12 col-md-8">
            <p>Fill in the form below to register your interest.</p>
<p>A member of our team will contact you with the required information.</p>
<h2>Contact:</h2>

<div id="formidable_container_2" class="formidable      bootstrap">

				    <form id="ff_2" name="contactform" method="post" action="contact-form.php">
                        
                        <div class="formidable_row row">
                        <div class="formidable_column col-sm-6">
                            <div>
                                 <div class="element form-group name-6">
                                    <label for="name-6">Name<span class="no_counter">*</span>
                                    </label>
                                    <input id="name-6" type="text" name="name" value=""  class="ccm-input-text" />

                                 </div>
                            </div>
                        </div>
                        <div class="formidable_column last col-sm-6">
                            <div>
                                <div class="element form-group phone-7">
                                    <label for="phone">Phone<span class="no_counter">*</span>
                                    </label>
                                    <input id="phone-7" type="tel" name="phone" value=""  class="ccm-input-tel" />
                                </div>
                            </div>
                        </div>
                        </div>
                        <div class="formidable_row row">
                            <div class="formidable_column last col-sm-12">
                                <div >
                                    <div class="element form-group email-8">
                                        <label for="email-8">Email<span class="no_counter">*</span>
                                         </label>
                                          <input id="email-8" type="email" name="email" value=""  class="ccm-input-email" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="formidable_row row">
                            <div class="formidable_column last col-sm-12">
                                 <div >
                                    <div class="element form-group your-message-9">
                                        <label for="your-message-9">Your message
                                        </label>
                                        <textarea id="your-message-9" name="message"  class="ccm-input-textarea"></textarea>
                                     </div>
                                </div>
                            </div>
                        </div>


                         <div class="formidable_row row">
                            <div class="element form-group form-actions">
                                <div class="col-xs-6">*(Required)</div>
                                <div id="ff_buttons" class="buttons col-xs-6">
                                   <div class="form-section">
                                    <button type="submit" name="submit" class="pull-right submit-btn-buyer" style="margin-top:10px; margin-right:14px; font-size:20px;color:white">Submit</button>
                                  </div>                    
                                   <div class="please_wait_loader"></div>
                                </div>
                            </div>
                        </div>
                </form>

</div>
        </div>
    </div>
</div>

<script>
    var initMap = function() {
        var styles = [ { "stylers": [ { "weight": 2 }, { "saturation": -100 } ] },{ } ]
        var latlng = new google.maps.LatLng(23.797150,90.404416);
        var options = {
            zoom: 15,
            center: latlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var map = new google.maps.Map(document.getElementById('map_canvas'), options);
        map.setOptions({styles: styles});
        new google.maps.Marker({
            position: latlng,
            map: map,
            icon: 'images/navication.png'
        });
    };
</script>


    </div>
 <?php include 'property/footer.php';?>

  <script>
    document.getElementById("contact").style.color = 'rgb(194, 171, 96)'; 
</script>