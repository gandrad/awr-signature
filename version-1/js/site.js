  jQuery(document).ready(function($) {


      // setInterval(function() {
      //     moveRight();
      // }, 6000);


      var slideCount = $('#slider ul li').length;
      var slideWidth = $('#slider ul li').width();
      var slideHeight = $('#slider ul li').height();
      var sliderUlWidth = slideCount * slideWidth;

      $('#slider').css({
          width: slideWidth,
          height: slideHeight
      });

      $('#slider ul').css({
          width: sliderUlWidth,
          marginLeft: -slideWidth
      });

      $('#slider ul li:last-child').prependTo('#slider ul');

      function moveLeft() {
          $('#slider ul').animate({
              left: +slideWidth
          }, 1, function() {
              $('#slider ul li:last-child').prependTo('#slider ul');
              $('#slider ul').css('left', '');
          });
      };

      function moveRight() {
          $('#slider ul').animate({
              left: -slideWidth
          }, 1, function() {
              $('#slider ul li:first-child').appendTo('#slider ul');
              $('#slider ul').css('left', '');
          });
      };

      $('a.control_prev').click(function() {
          moveLeft();
      });

      $('a.control_next').click(function() {
          moveRight();
      });

  });

  // DOM Ready
  $(function() {

      var $el, leftPos, newWidth;
      $mainNav2 = $("#example-two");

      /*
          EXAMPLE ONE
      */

      /* Add Magic Line markup via JavaScript, because it ain't gonna work without */
      $("#example-one").append("<li id='magic-line'></li>");

      /* Cache it */
      var $magicLine = $("#magic-line");

      $magicLine
          .width($(".current_page_item").width())
          .css("left", $(".current_page_item a").position().left)
          .data("origLeft", $magicLine.position().left)
          .data("origWidth", $magicLine.width());

      $("#example-one li").find("a").hover(function() {
          $el = $(this);
          leftPos = $el.position().left;
          newWidth = $el.parent().width();

          $magicLine.stop().animate({
              left: leftPos,
              width: newWidth
          });
      }, function() {
          $magicLine.stop().animate({
              left: $magicLine.data("origLeft"),
              width: $magicLine.data("origWidth")
          });
      });




      /*
          EXAMPLE TWO
      */

      $mainNav2.append("<li id='magic-line-two'></li>");

      var $magicLineTwo = $("#magic-line-two");

      $magicLineTwo
          .width($(".current_page_item_two").width())
          .height($mainNav2.height())
          .css("left", $(".current_page_item_two a").position().left)
          .data("origLeft", $(".current_page_item_two a").position().left)
          .data("origWidth", $magicLineTwo.width())
          .data("origColor", $(".current_page_item_two a").attr("rel"));

      $("#example-two a").hover(function() {
          $el = $(this);
          leftPos = $el.position().left;
          newWidth = $el.parent().width();
          $magicLineTwo.stop().animate({
              left: leftPos,
              width: newWidth,
              backgroundColor: $el.attr("rel")
          })
      }, function() {
          $magicLineTwo.stop().animate({
              left: $magicLineTwo.data("origLeft"),
              width: $magicLineTwo.data("origWidth"),
              backgroundColor: $magicLineTwo.data("origColor")
          });
      });

      /* Kick IE into gear */
      $(".current_page_item_two a").mouseenter();

  });





  (function() {
      function id(v) {
          return document.getElementById(v);
      }

      function loadbar() {
          var ovrl = id("overlay"),
              prog = id("progress"),
              stat = id("progstat"),
              img = document.images,
              c = 0;
          tot = img.length;

          function imgLoaded() {
              c += 1;
              var perc = ((100 / tot * c) << 0) + "%";
              prog.style.width = perc;
              stat.innerHTML = "Loading " + perc;
              if (c === tot) return doneLoading();
          }

          function doneLoading() {
              ovrl.style.opacity = 0;
              setTimeout(function() {
                  ovrl.style.display = "none";
              }, 1200);
          }
          for (var i = 0; i < tot; i++) {
              var tImg = new Image();
              tImg.onload = imgLoaded;
              tImg.onerror = imgLoaded;
              tImg.src = img[i].src;
          }
      }
      document.addEventListener('DOMContentLoaded', loadbar, false);
  }());