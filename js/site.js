if (typeof(initMap) !== 'function') {
    var initMap = function() {
        return false;
    };
}
$(document).ready(function() {
    $('#about-us #content h2:first').addClass('open');
    $('#about-us #content h2').not(':first').nextUntil('h2').hide();
    $('#about-us #content h2').on('click', function() {
        if ($(this).hasClass('open')) {
            return false;
        }
        var that = $(this);
        $('#about-us #content h2.open').nextUntil('h2').slideToggle('fast', function() {
            $('#about-us #content h2').not(that).removeClass('open');
            that.toggleClass('open');
            that.nextUntil('h2').slideToggle('slow');
        });
    });

    $('.mobile-menu-btn').on('click', function() {
        $('.nav').toggleClass('open');
    });

    $('a.toggle').on('click', function() {
        var target = $(this).attr('href');
        $(target).slideToggle();
        return false;
    });
});