// create the module and name it scotchApp
    // also include ngRoute for all our routing needs
var awr = angular.module('awr', ['ngRoute']);

// configure our routes
Basic.config(function($routeProvider, $locationProvider) {
    $routeProvider

        // route for the home page
        .when('/', {
            templateUrl : 'pages/home.html',
            controller  : 'mainController'
        });


        // route for the contact page

        $locationProvider.html5Mode(true);
});


Basic.controller('mainController', function($scope) {
    $scope.message = 'Contact us! JK. This is just a demo.';
});
