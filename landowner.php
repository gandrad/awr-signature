<?php include 'property/header.php';?>


    <div id="content">
        <div class="row form-view">
            <form name="contactform" method="post" action="landowner-form.php">
                <h1 style="text-align: center;font-size: 33px;color: white;font-weight: 100;color: rgb(194, 171, 96);padding-bottom: 35px;font-family: 'Source Sans Pro';">Let Us Guide You</h1>
                <hr/>
                <p>

                The project is located in <input type="text" placeholder="Locality" name="p1"> and the property address is <input type="text" placeholder="Address Details" name="p2">. The project has a plot size of <input type="text" placeholder="Size/Area of the Plot" name="p3">, <input type="text" placeholder="Dimension (Length in Feet)" name="p4"> in length and <input type="text" placeholder="Dimension (Breadth in Feet)" name="p5"> in breadth with a front road of <input type="text" placeholder="Width of the Road*" name="p6"> The land is
                <select class="sec" name="p7" id="selectionbox">
                                        <option value="volvo">Category of Land</option>
                                        <option value="saab">FREEHOLD</option>
                                        <option value="mercedes">LEASEHOLD</option>
                </select>,
                <select name="t8">
                                        <option value="volvo">Type of The Land</option>
                                        <option value="saab">Residential</option>
                                        <option value="mercedes">Commercial</option>
                                        <option value="mercedes">Others</option>
                </select>
                and <input type="text" placeholder="Facing of the Property" name="p9"> facing.
                <h6>CONTACT INFORMATION</h6>
                <hr/>
                <p>
                    I am <input type="text " placeholder="Name* " name="p10" required> and i am a <input type="text " placeholder="Profession* " name="p11" required>. I can be reached at <input type="text " placeholder="Mobile Number " name="p12"> and emailed at <input type="email" placeholder="E-mail Address* " name="p13" required> and mailed at <input type="text " placeholder="Mailing Address* " name="p14" required>
                </p>

                <p>
                  <div class="form-section">
                    <button type="submit" name="submit" class="pull-right submit-btn-buyer" style="margin-top:10px; margin-right:14px; font-size:20px;color:white">Submit</button>
                    </div>
                </p>

            </form>
        </div>
<!--
        <div class="container-fluid">
            <div class="row" id="mixitup">
            </div>
        </div>
 -->

    </div>
 <?php include 'property/footer.php';?>

 <script>
    document.getElementById("landowner").style.color = 'rgb(194, 171, 96)'; 
</script>
